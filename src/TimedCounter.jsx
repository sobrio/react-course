import React, { Component } from 'react'
import RenderCounter from './RenderCounter'


class TimedCounter extends Component {
    constructor(props){
        super(props)
        this.state = {
            counter: props.initialValue || 0
        }
        setTimeout(() => {
            this.setState( state => {
                return {
                    counter: state.counter + 1
                }
            })
        }, props.timeout || 3000)
    }
    render(){
        return <RenderCounter counter={this.state.counter} />
    }
}

export default TimedCounter