import React, { Component } from 'react'

class Sum extends Component {
    render(){
        return <h1>Sum: {this.props.numbers.reduce((acc,curr) => acc+curr)}</h1>
    }
}

export default Sum