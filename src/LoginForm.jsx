import React, { Component } from 'react';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            session: false
        }
    }
    changeHandler = (event) => {
        let { name, value, checked, type } = event.target
        this.setState({
            [name]: (type==='checkbox') ? checked : value
        })
    }
    render() {
        let { email, password, session } = this.state 
        return (
            <>
                <label htmlFor="email">Email</label>
                <input onChange={this.changeHandler} id="email" type="email" name="email" value={email}/>
                <label htmlFor="password">Email</label>
                <input onChange={this.changeHandler} id="password" type="password" name="password" value={password}/>
                <label htmlFor="session">
                    <input onChange={this.changeHandler} id="session" type="checkbox" name="session" checked={session}/> Mantieni aperta la sessione
                </label>
            </>
        )
    }
}
 
export default LoginForm