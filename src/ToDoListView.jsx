import React from 'react'

const ToDoListView = ({todos, onRemoveToDo}) => (
    <ul>
        {todos.map( (todo, i) => (
            <li key={i}>{todo} <button onClick={() => onRemoveToDo(i)}>Cancel</button> </li>
        ))}
    </ul>
)

export default ToDoListView