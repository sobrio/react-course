import { useState, useEffect, useCallback } from 'react'

const useGeolocation = (auto) => {
    const [coordinates, setCoordinates] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState({ status: false, message: '' })

    const getCoordinates = useCallback(() => {    
        if("geolocation" in navigator) {
            setLoading(true)
            navigator.geolocation.getCurrentPosition(
                position => {
                    let {latitude, longitude} = position.coords
                    setCoordinates([latitude, longitude])
                    setLoading(false)
                },
                error => {
                    setError({status:true, message: error.message})
                    setLoading(false)
                }
            )
        } else {
            setError({status:true, message:'Geolocation is unavailable in your browser'})
        }
    },[])

    useEffect(()=>{
        if(auto){
            getCoordinates()
        }
    },[auto])

    return [coordinates, loading, error, getCoordinates]
}

export default useGeolocation