import React from 'react'
import styled from 'styled-components'
import ToDoListView from './ToDoListView'

const TodoContainer = styled.div`
	margin: 10px;
	padding: 10px;
	border: 1px solid #333;
	box-shadow: 0 2px 2px #333;

	li,
	button,
	input {
		font-size: 0.5em;
	}
`

export default class TodoList extends React.Component {
	state = {
		todos: [],
		newTodo: '',
	}

	handleInputChange = event => {
		this.setState({
			newTodo: event.target.value,
		})
	}

	handleAddTodo = () => {
		this.setState(state => {
			return {
				todos: [...state.todos, state.newTodo],
				newTodo: '',
			}
		})
    }
    
    handleRemoveToDo = (index) => {
        this.setState( prevState => {
            return {
                todos: prevState.todos.filter( (todo, i) => {
                    return i !== index
                })
            }
        })
    }

	render() {
		const { todos, newTodo } = this.state

		return (
			<TodoContainer>
				<input name="newTodo" value={newTodo} onChange={this.handleInputChange} />
				<button onClick={this.handleAddTodo}>Add</button>
				<ToDoListView todos={todos} onRemoveToDo={this.handleRemoveToDo} />
			</TodoContainer>
		)
	}
}
