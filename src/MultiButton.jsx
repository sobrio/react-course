import React, { Component } from 'react'

class MultiButton extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            pressedButton: "Uno",
            buttons: [
                "Uno", "Due", "Tre"
            ]
        }
    }
    handleButtonClick = (e) => {
        let { name } = e.target
        this.setState({
            pressedButton: name
        })
    }
    render() { 
        return (
            <>             
                {
                    this.state.buttons.map((button, i) => {
                        return (
                            <button 
                                key={i} 
                                name={button} 
                                onClick={this.handleButtonClick}>
                                {button}
                            </button>
                        )
                    })
                }
                <p>Ultimo bottone premuto: {this.state.pressedButton}</p>
            </>
        );
    }
}
 
export default MultiButton;