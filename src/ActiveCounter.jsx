import React, { Component } from 'react'
import ActiveCounterView from './ActiveCounterView'

class ActiveCounter extends Component {
    constructor(props){
        super(props)
        this.state = {
            counter: props.initialValue || 0
        }
    }
    handleIncrement = () => {
        this.setState(prevState => ({ counter: prevState.counter + 1}))
    }
    handleDecrement = () => {
        this.setState(prevState => ({ counter: prevState.counter - 1}))
    }
    handleReset = () => {
        this.setState({
            counter: this.props.initialValue || 0
        })
    }
    render(){
        return (
            <>
                <ActiveCounterView 
                    counter={this.state.counter} 
                    onIncrement={this.handleIncrement} />
                <button onClick={this.handleDecrement}>Decrement</button>
                <button onClick={this.handleReset}>Reset</button>
            </>
        )
    }
}

export default ActiveCounter