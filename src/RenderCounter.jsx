import React, { Component } from 'react'

class RenderCounter extends Component {
    render(){
        return <h1>Count: {this.props.counter}</h1>
    }
}

export default RenderCounter