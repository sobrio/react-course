import React, { Component } from 'react'

class ActiveCounterView extends Component {
    render(){
        return (
            <>
                <h1>Count: {this.props.counter}</h1>
                <button onClick={this.props.onIncrement}>Increment</button>
            </>
        )
    }
}

export default ActiveCounterView