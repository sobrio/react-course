import React, { useState, useEffect } from 'react'

const Planets = () => {
    const [loading, setLoading] = useState(false)
    const [planets, setPlanets] = useState([])

    useEffect(() => {
        setLoading(true)
        fetch('https://swapi.co/api/planets')
        .then( response => response.json())
        .then( json => {
            let { results } = json
            setPlanets(results)
            setLoading(false)
            console.log(results)
        })
    }, [])

    if(loading){
        return <h1>Loading</h1>
    } else {
        return (
            <>
                <h1>Star wars planets!</h1>
                <ul>
                    {
                        planets.map( (planet, i) => <li key={i}>Name: {planet.name}<br />Rotation period: {planet.rotation_period}</li> )
                    }
                </ul>
            </>
        )
    }
}

export default Planets