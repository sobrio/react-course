import React, { Component } from 'react'
import ReactDOM from 'react-dom'
//import HelloWorld from './HelloWorld'
//import TimedCounter from './TimedCounter'
//import ActiveCounter from './ActiveCounter'
//import MultiButton from './MultiButton'
//import LoginForm from './LoginForm'
//import TodoList from './ToDoList'
//import HookCounter from './HookCounter'
//import Planets from './Planets'
import Geolocation from './Geolocation'

class App extends Component {
    render() {
        return (
            <>
                <Geolocation />
            </>
        )
    }
}

const root = document.getElementById("root")
ReactDOM.render(<App />, root)