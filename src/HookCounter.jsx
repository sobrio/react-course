import React, { useState } from 'react'

const HookCounter = ({initialValue}) => {
    const [counter, setCounter] = useState(initialValue)
    return (
        <>
            {counter}
            <button onClick={() => setCounter(counter+1)}>Increment</button>
            <button onClick={() => setCounter(counter-1)}>Decrement</button>
            <button onClick={() => setCounter(initialValue)}>Reset</button>
        </>
    )
}

export default HookCounter