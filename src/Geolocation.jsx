import React from 'react'
import useGeolocation from './customHooks/useGeolocation'

const Geolocation = () => {
    const auto = false;
    const [coordinates, loading, error, getCoordinates] = useGeolocation(auto)
    if(loading) {
        return <p>Loading</p>
    } else {      
        if(error.status) {
            return <p>Error: {error.message}</p>
        } else {
            return (
                <>
                    { 
                        !auto && <button onClick={()=>getCoordinates()}>Get coordinates</button>
                    }
                    <h1>Coordinates: {coordinates.join()}</h1>
                </>
            )         
        }
    }   
}

export default Geolocation