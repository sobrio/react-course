import React from 'react'

const SumAsFunction = ({numbers}) => (
    <h1>Sum: {numbers.reduce((acc,curr) => acc+curr)}</h1>
)

export default SumAsFunction